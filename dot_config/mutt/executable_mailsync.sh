#!/usr/bin/env sh

# Run only if user logged in (prevent cron errors)
pgrep -u "$USER" >/dev/null || { echo "$USER not logged in; sync will not run."; exit ;}
# Run only if not already running in other instance
pgrep -x mbsync >/dev/null && { echo "mbsync is already running." ; exit ;}
# Checks for internet connection and set notification script.
ping -q -c 1 1.1.1.1 > /dev/null || { echo "No internet connection detected."; exit ;}
command -v notify-send >/dev/null || echo "Note that \`libnotify\` or \`libnotify-send\` should be installed for pop-up mail notifications with this script."

# Command to be run to send new mail notification
notify() { notify-send "Mail-Sync" "📬 $1" ;}

# Run MBSYNC
mbsync --config /home/$USER/.config/mutt/mbsyncrc --all

# Index with MU
mu index --maildir="/home/$USER/.local/share/mail"

# Get the number of unread mails today
todaycount=$(mu find flag:unread date:today --quiet | wc -l)
if ![ "$todaycount" -eq "0" ]; then
    notify "You have $todaycount unread email(s)"
fi
