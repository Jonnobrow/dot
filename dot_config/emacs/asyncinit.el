(require 'package)
(setq package-enable-at-startup nil)
(package-initialize)

(require 'org)
(require 'ox)
(require 'cl)
(setq org-export-async-debug t)
(setq org-export-with-smart-quotes t)
(setq org-latex-compiler "latexmk")
(setq org-latex-pdf-process
      '("latexmk -pdf --synctex=1 -interaction=nonstopmode -shell-escape %f"
        "latexmk -pdf --synctex=1 -interaction=nonstopmode -shell-escape %f"
        "latexmk -pdf --synctex=1 -interaction=nonstopmode -shell-escape %f"))
(setq org-src-preserve-indentation t)
(setq org-latex-listings 'minted)
(setq org-latex-minted-options '(("frame" "lines")
                                 ("fontsize" "\\scriptsize")
                                 ("linenos" "")
                                 ("breaklines")))
(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))
(setq org-latex-default-class "article")
(add-to-list 'org-latex-classes
  '("article"
    "\\documentclass[10pt,a4paper,dvipsnames]{article}"
    ("\\section{%s}" . "\\section*{%s}")
    ("\\subsection{%s}" . "\\subsection*{%s}")
    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
    ("\\paragraph{%s}" . "\\paragraph*{%s}")
    ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(add-to-list 'org-latex-packages-alist '("" "xcolor"))
(add-to-list 'org-latex-packages-alist '("" "mathtools"))
(add-to-list 'org-latex-packages-alist '("" "tikz"))
(add-to-list 'org-latex-packages-alist '("" "fullpage"))
